﻿using GraphQL.Types;
using GraphQl_solution.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarvedRock.Api.GraphQL
{
    public class AuthorInputType : InputObjectGraphType<Author>
    {
        public AuthorInputType()
        {
            Name = "AuthorInput";
            Field<NonNullGraphType<IdGraphType>>("id");   
            Field<NonNullGraphType<StringGraphType>>("name");
        }
    }
}
