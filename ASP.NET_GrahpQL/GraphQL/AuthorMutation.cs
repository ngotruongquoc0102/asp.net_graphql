﻿using CarvedRock.Api.Domain;
using GraphQL;
using GraphQL.Types;
using GraphQl_solution.Database;
using GraphQl_solution.GraphQL;
using MediatR;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CarvedRock.Api.GraphQL
{
    public class AuthorMutation : ObjectGraphType
    {
        public AuthorMutation(IMediator mediator)
        {
            Name = "Mutation";
            Field<AuthorType>(
                "createAuthor",
                arguments: new QueryArguments(
                    new QueryArgument<NonNullGraphType<AuthorInputType>> { Name = "author" }
                    ),
                resolve: context =>
                {
                    var author = context.GetArgument<object>("author");
                    var authorDto = JToken.FromObject(author).ToObject<AuthorDto>();
                    try
                    {
                       var res =  mediator.Send(new CreateAuthorCommand(authorDto)).Result;
                        return res;
                    } catch (Exception e)
                    {
                        Debug.WriteLine("Expected Error Happened here " + e.Message);
                        throw new Exception(e.Message);
                    }
                });
        }
    }
}
