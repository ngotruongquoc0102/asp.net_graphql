﻿using CarvedRock.Api.Domain;
using GraphQl_solution.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarvedRock.Api.Dxos
{
    public interface IAuthorDxos
    {
        AuthorDto MapAuthorDto(Author author);
    }
}
