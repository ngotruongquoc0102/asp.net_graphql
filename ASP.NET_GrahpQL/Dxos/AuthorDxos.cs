﻿using AutoMapper;
using CarvedRock.Api.Domain;
using GraphQl_solution.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarvedRock.Api.Dxos
{
    public class AuthorDxos : IAuthorDxos
    {
        private readonly IMapper _mapper;

        public AuthorDxos()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Author, AuthorDto>()
                    .ForMember(dst => dst.Id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dst => dst.Name, opt => opt.MapFrom(src => src.Name));
            });

            _mapper = config.CreateMapper();
        }

        public AuthorDto MapAuthorDto(Author author)
        {
            return _mapper.Map<Author, AuthorDto>(author);
        }
    }
}
