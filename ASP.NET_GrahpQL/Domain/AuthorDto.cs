﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarvedRock.Api.Domain
{
    public class AuthorDto
    {
        [JsonProperty("id")]
        public  int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
