﻿using GraphQl_solution.Database;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CarvedRock.Api.Domain
{
    public class CreateAuthorCommand: IRequest<Task<AuthorDto>>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public CreateAuthorCommand(AuthorDto author)
        {
            Id = author.Id;
            Name = author.Name;
        }
    }
}
